INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_CNAP_CPSS cnap_cpss)

FIND_PATH(
    CNAP_CPSS_INCLUDE_DIRS
    NAMES cnap_cpss/api.h
    HINTS $ENV{CNAP_CPSS_DIR}/include
        ${PC_CNAP_CPSS_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    CNAP_CPSS_LIBRARIES
    NAMES gnuradio-cnap_cpss
    HINTS $ENV{CNAP_CPSS_DIR}/lib
        ${PC_CNAP_CPSS_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CNAP_CPSS DEFAULT_MSG CNAP_CPSS_LIBRARIES CNAP_CPSS_INCLUDE_DIRS)
MARK_AS_ADVANCED(CNAP_CPSS_LIBRARIES CNAP_CPSS_INCLUDE_DIRS)

